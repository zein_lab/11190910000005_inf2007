package id.zein_lab.tugas.uas.model;



import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class Aset implements Serializable {

    private static final long serialVersionUID = -6756463875294313469L;
    private int kondisiBrg;
    private LocalDateTime waktuTerima;
    private LocalDateTime waktuGudang;
    private String namaBrg;
    private boolean keluar = false;
    private int jumlah;
    private String kodeBrg;
    public Aset(){
        
    }

    public Aset(int kondisiBrg, LocalDateTime waktuTerima, LocalDateTime waktuGudang, String namaBrg, int jumlah) {
        this.kondisiBrg = kondisiBrg;
        this.waktuTerima = waktuTerima;
        this.waktuGudang = waktuGudang;
        this.namaBrg = namaBrg;
        this.jumlah = jumlah;
        this.kodeBrg = kodeBrg;
        
    }

    public int getKondisiBrg() {
        return kondisiBrg;
    }

    public String getKodeBrg() {
        return kodeBrg;
    }

    public void setKodeBrg(String kodeBrg) {
        this.kodeBrg = kodeBrg;
    }

    public void setKondisiBrg(int kondisiBrg) {
        this.kondisiBrg = kondisiBrg;
    }

    public LocalDateTime getWaktuTerima() {
        return waktuTerima;
    }

    public void setWaktuTerima(LocalDateTime waktuTerima) {
        this.waktuTerima = waktuTerima;
    }

    public LocalDateTime getWaktuGudang() {
        return waktuGudang;
    }

    public void setWaktuGudang(LocalDateTime waktuGudang) {
        this.waktuGudang = waktuGudang;
    }

    public String getNamaBrg() {
        return namaBrg;
    }

    public void setNamaBrg(String namaBrg) {
        this.namaBrg = namaBrg;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
    @Override
    public String toString(){
        return "Aset{" + "Kode Barang =" + kodeBrg + ", Nama Barang" + namaBrg + ", Jumlah Barang " + jumlah
                + ", Waktu Terima Barang =" + waktuTerima + ", Waktu Gudangkan Barang =" + waktuGudang
                + ", Kondisi Barang" + kondisiBrg + '}';
    }
    

}
