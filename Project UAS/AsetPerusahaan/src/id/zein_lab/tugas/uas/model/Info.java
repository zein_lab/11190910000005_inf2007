package id.zein_lab.tugas.uas.model;



/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class Info {

    private final String aplikasi = "Program Pendataan Aset Sederhana";
    private final String version = "Versi 1.0.0 (Beta)";

    public String getAplikasi() {
        return aplikasi;

    }

    public String getVersion() {
        return version;
    }

}
