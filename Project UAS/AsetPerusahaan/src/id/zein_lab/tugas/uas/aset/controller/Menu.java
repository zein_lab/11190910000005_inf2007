package id.zein_lab.tugas.uas.aset.controller;



import id.zein_lab.tugas.uas.model.Info;
import java.util.Scanner;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();
        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");
        System.out.println("SILAHKAN PILIH OPSI YANG ANDA INGINKAN");
        System.out.println("1. Penerimaan Barang");
        System.out.println("2. Pemindahan Barang ke Gudang");
        System.out.println("3. Laporan Aset");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.println("Pilih Opsi (1/2/3/4/5)");
        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Opsi (1/2/3/4/5) = ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        AsetController ac = new AsetController();
        switch (noMenu) {
            case 1:
                ac.setMasukBarang();
                break;
            case 2:
                ac.setGudangkanBarang();
                break;
            case 3:
                ac.getDataAset();
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}
