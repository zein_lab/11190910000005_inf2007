

import com.google.gson.Gson;
import id.zein_lab.tugas.uas.model.Aset;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class AsetController {

    private static final String FILE = "C:\\Users\\AWIEZ FATHWA ZEIN\\Documents\\AsetPerusahaan\\lib\\aset.json";
    private Aset aset;
    private Scanner in;
    private String kodeBrg;
    private String namaBrg;
    private String kondisiBrg;
    private int jumlah;
    private final LocalDateTime waktuTerima;
    private LocalDateTime waktuGudang;
    private LocalDateTime waktuKeluar;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;
    private boolean isKeluar;

    public AsetController() {
        in = new Scanner(System.in);
        waktuTerima = LocalDateTime.now();
        waktuGudang = LocalDateTime.now();
        waktuKeluar = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("dd-MM-yyy HH:mm:ss");
    }

    public void setMasukBarang() {
        System.out.println("Masukkan Kode Barang = ");
        kodeBrg = in.next();
        System.out.println("Masukkan Nama Barang = ");
        namaBrg = in.next();
        System.out.println("Kondisi Barang 1 = Baru 2 = Rusak");
        System.out.print("Masukkan Kondisi Barang = ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Masukkan Kondisi barang = ");
        }
        kondisiBrg = in.next();
        System.out.println("Jumlah Barang = ");
        jumlah = in.nextInt();
        String formatWaktuTerima = waktuTerima.format(dateTimeFormat);
        System.out.println("Waktu Terima Barang = " + formatWaktuTerima);
        aset = new Aset();
        aset.setKodeBrg(kodeBrg.toUpperCase());
        aset.setNamaBrg(namaBrg);
        aset.setWaktuTerima(waktuTerima);
        aset.setJumlah(jumlah);
        setWriteAset(FILE, aset);
        System.out.println("Apakah Ingin Transaksi Kembali?");
        System.out.print("1) Ya, 2) Tidak = ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMasukBarang();
        }

    }

    public void setWriteAset(String file, Aset aset) {
        Gson gson = new Gson();
        List<Aset> asets = getReadAset(file);
        asets.remove(aset);
        asets.add(aset);
        String json = gson.toJson(asets);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(AsetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setGudangkanBarang() {
        System.out.print("Masukkan Kode Barang");
        kodeBrg = in.next();
        Aset a = getSearch(kodeBrg);
        if (a != null) {
            LocalDateTime tempWaktu = LocalDateTime.from(a.getWaktuTerima());
            waktuGudang = LocalDateTime.now();
            long jam = tempWaktu.until(waktuGudang, ChronoUnit.HOURS);
            a.setWaktuGudang(waktuGudang);
            System.out.println("Kode Barang = " + a.getKodeBrg());
            System.out.println("Nama Barang = " + a.getNamaBrg());
            System.out.println("Kondisi Barang = " + a.getKondisiBrg());
            System.out.println("Jumlah Barang = " + a.getJumlah());
            System.out.println("Waktu Terima = " + a.getWaktuTerima().format(dateTimeFormat));
            System.out.println("Waktu Pemindahan ke Gudang = " + a.getWaktuGudang().format(dateTimeFormat));
            System.out.println("PROSES PEMINDAHAN BARANG ?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar = ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    a.setKeluar(true);
                    setWriteAset(FILE, a);
                    break;
                case 2:
                    setGudangkanBarang();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;

            }
            System.out.println("Apakah Ingin Memproses Kembali?");
            System.out.print("1) Ya, 2) Tidak = ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setGudangkanBarang();
            }
        } else {
            System.out.println("Data Tidak Ditemukan");
            setGudangkanBarang();
        }

    }

    public Aset getSearch(String kodeBrg) {
        List<Aset> asets = getReadAset(FILE);
        Aset a = asets.stream()
                .filter(aa -> kodeBrg.equalsIgnoreCase(aa.getKodeBrg()))
                .findAny()
                .orElse(null);
        return a;
    }

    public List<Aset> getReadAset(String file) {
        List<Aset> asets = new ArrayList<>();
        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Aset[] as = gson.fromJson(line, Aset[].class);
                asets.addAll(Arrays.asList(as));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AsetController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AsetController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return asets;
    }

    public void getDataAset() {
        List<Aset> asets = getReadAset(FILE);
        Predicate<Aset> isKeluar = e -> e.isKeluar() == true;
        Predicate<Aset> isDate = e
                -> e.getWaktuTerima().toLocalDate().equals(LocalDate.now());
        List<Aset> aResults
                = asets.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        System.out.println("Kode Barang \t Nama Barang \t Jumlah \t Waktu Terima \t\t Waktu Pemindahan ke Gudang");
        System.out.println("-------------\t-----------\t\t\t---------------------\t------------------------");
        aResults.forEach((a) -> {
            System.out.println(a.getKodeBrg() + "\t\t" + a.getNamaBrg() + "\t\t" + a.getJumlah() + "\t\t" + a.getWaktuTerima().format(dateTimeFormat) + "\t" + a.getWaktuGudang().format(dateTimeFormat) + "\t" + a.getKondisiBrg());
        });
        System.out.println("----------\t-----------\t\t\t---------------------\t-----------------------");
        System.out.println("============================");
        
        Menu m = new Menu();
        m.getMenuAwal();
    }

}
