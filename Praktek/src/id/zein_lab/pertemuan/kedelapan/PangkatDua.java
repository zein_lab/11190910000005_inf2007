package id.zein_lab.pertemuan.kedelapan;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class PangkatDua {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int i, k;
        k = 0;
        for (i = 0; i < array.length; i++) {
            k = 1 + i;
            array[i] = k * k;
            System.out.println("pangkat " + (i + 1) + " = " + array[i]);

        }
    }

}
