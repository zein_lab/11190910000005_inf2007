package id.zein_lab.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class ArrayHitungRata {

    public static void main(String[] args) {
        int[] x = new int[6];
        int i;
        float u, jumlah;
        Scanner in = new Scanner(System.in);
        for (i = 0; i < x.length; i++) {
            x[i] = in.nextInt();

        }
        jumlah = 0;
        for (i = 0; i < x.length; i++) {
            jumlah = jumlah + x[i];
        }
        u = jumlah / 6;
        System.out.println("Hasilnya adalah " + u);
    }
}
