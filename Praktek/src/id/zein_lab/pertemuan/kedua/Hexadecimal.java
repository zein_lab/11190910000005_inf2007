package id.zein_lab.pertemuan.kedua;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class Hexadecimal {
    public static void main(String args[]) {
        int x = 0x0001;
        int y = 0x7fffffff;
        int z = 0xDeadCafe;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.print("z = " + z);
    }
    
}
