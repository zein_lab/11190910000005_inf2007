package id.zein_lab.pertemuan.kedua;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class Assign {

    public static void main(String args[]) {
        boolean b = false; //boolean hanya true dan false
        System.out.println("Value b = "+b);
        char c = 'C'; //char menampung 1 karakter
        System.out.println("Value c = " + c);
        char bt = 10;
        System.out.println("Value bt = " + bt);
        short s = 32767; // -32768 sd 32767
        System.out.println("Value s = " + s);
        int i = 30; //-2147483648 sd 2147483647
        System.out.println("Value i = " + i);
        long l = 40L; //-92233720368
        System.out.println("Value l = " + l);
        float f = 3.14F;
        System.out.println("Value f = " + f);
        double d = 3.14;
        System.out.println("Value d = " + d);
    }

}
