package id.zein_lab.pertemuan.kedua;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
import java.util.Scanner;
/*PROGRAM FahrenheitCelcius*/
/*Program Untuk mencetak tabel Fahrenheit-Celcius dari x sa,pai y
dengan kenaikan sebesar step. Masukan program ini adalah suhu awal,
suhu akhir, step, dan keluarannya adalah tabel konversi suhu dalam C dan F*/
public class FahrenheitCelcius {
    public static void main(String[] args) {
		/*deklarasi*/
		float F, C;
		int x, y, step;
		
		/*ALGORITMA*/
		Scanner in = new Scanner(System.in);
		x = in.nextInt();
		y = in.nextInt();
		step = in.nextInt();
		
		F = x;
		while (F <= y) {
			System.out.println("5/9 : " + 5/9);
			C = (5/9) + (F-32) / 9;
			System.out.printf("%3.0f %6.1f \n", F, C);
			F = F + step;
                }
    
        }
}
