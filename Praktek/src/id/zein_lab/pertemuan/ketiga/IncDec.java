package id.zein_lab.pertemuan.ketiga;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class IncDec {
    public static void main(String[] args){
        int x = 8, y = 13;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("x = " + ++x);
        System.out.println("y = " + y++);
        System.out.println("x = " + x--);
        System.out.println("y = " + --y);
    }
    
}
