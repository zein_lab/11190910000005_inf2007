package id.zein_lab.pertemuan.kelima;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class CetakBanyakHelloWorldRepeat {
    public static void main(String[] args) {
        int i = 1;
        do {
            System.out.println("Hello, World");
            i = i + 1;
        } while (i < 10);
    }

}
