package id.zein_lab.pertemuan.kelima;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class CetakBanyakHelloWorldWhile {
    public static void main(String[] args) {
        int i;
        i = 1;
                
        while (i <= 10) {
            System.out.println("Hello, World");
            i = i + 1;
        }
    }

}
