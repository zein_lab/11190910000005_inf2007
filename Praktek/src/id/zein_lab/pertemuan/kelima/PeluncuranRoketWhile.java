package id.zein_lab.pertemuan.kelima;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class PeluncuranRoketWhile {
    public static void main(String[] args) {
        int i = 100;
        while (i >= 0) {
            System.out.println(i);
            i = i - 1;
        }
        System.out.println("Go");
    }

}
