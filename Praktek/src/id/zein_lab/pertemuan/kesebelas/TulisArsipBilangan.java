package id.zein_lab.pertemuan.kesebelas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class TulisArsipBilangan {

    public static void main(String[] args) {
        int i, n;
        Scanner in = new Scanner(System.in);

        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream("C:\\Users\\AWIEZ FATHWA ZEIN\\Documents\\Materi Kuliah\\Temp\\Tulis.txt"));
            System.out.print("masukkan n: ");
            n = in.nextInt();

            for (i = 1; i <= n; i++) {
                outFile.println(i);
            }
            outFile.close();

        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }

}
