package id.zein_lab.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class BacaArsipBilangan {

    public static void main(String[] args) {
        int x;
        try {
            File file = new File("C:\\Users\\AWIEZ FATHWA ZEIN\\Documents\\Materi Kuliah\\Temp\\Tulis.txt");
            BufferedReader print = new BufferedReader(new FileReader(file));
            Scanner line = new Scanner(print);
            while (line.hasNextInt()) {
                x = line.nextInt();
                System.out.println(x);
            }
            print.close();
        } catch (IOException e) {
            System.out.println("Error I/O : " + e);
        }
    }
}
