package id.zein_lab.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class KataTerpanjang {
    String getCariKataTerpanjang(String file) {
        String[] L = new String[100];
        int n = 0;
        String Maks = null;

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            Scanner line = new Scanner(br);
            while (line.hasNext()) {
                L[n] = line.next();
                n++;
            }

            Maks = L[0];
            for (int i = 0; i < n - 1; i++) {
                if (Maks.length() < L[i + 1].length()) {
                    Maks = L[i + 1];
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(KataTerpanjang.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Maks;
    }

    public static void main(String[] args) {
        String Kalimat;

        String File = "C:\\Users\\AWIEZ FATHWA ZEIN\\Documents\\Materi Kuliah\\Temp\\Longest.txt";
        Scanner in = new Scanner(System.in);
        KataTerpanjang baca = new KataTerpanjang();

        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream(File));

            System.out.print("Masukkan Kalimat: ");
            Kalimat = in.nextLine();
            
            outFile.println(Kalimat);
            outFile.close();
            
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        
        System.out.println("\nKata Terpanjang = " + baca.getCariKataTerpanjang(File));
    }


}
