package id.zein_lab.pertemuan.keempat;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
import java.util.Scanner;
public class UpahKaryawan {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Nama :");
        String nama = in.next();
        System.out.printf("Jam Lembur :");
        int JJK = in.nextInt();
        int lembur, upah; 
        if (JJK <= 48){
            upah = JJK * 2000;
        }else{
            lembur = JJK - 48;
            upah = 48 * 2000 + lembur * 3000;
        }
        System.out.println(nama + " " + upah);
    }
    
}
