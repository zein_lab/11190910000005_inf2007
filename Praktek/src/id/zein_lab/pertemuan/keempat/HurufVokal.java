package id.zein_lab.pertemuan.keempat;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
import java.util.Scanner;
public class HurufVokal {
    public static void main(String [] arguments){
        char huruf;
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Huruf : ");
        huruf = input.next().charAt(0);
        if (huruf == 'a' ||huruf == 'i'|| huruf == 'u'|| huruf == 'e'|| huruf =='o'){
            System.out.println(huruf + " Termasuk Jenis Huruf Vokal ");
        }
        else{
            System.out.println(huruf + " Termasuk Jenis Huruf Konsonan ");
        }
        
        
    }
    
}
