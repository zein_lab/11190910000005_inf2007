package id.zein_lab.pertemuan.keempat;

/**
 *import.java.util.scanner;
 * @author AWIEZ FATHWA ZEIN
 */
import java.util.Scanner;
public class BilanganGenap {
    public static void main(String args[]){
        Scanner s=new Scanner(System.in);
        System.out.printf("Input Sebuah Bilangan : ");
        int bilangan=s.nextInt();
        if (bilangan % 2 == 1) {
            System.out.printf(bilangan+" Adalah bilangan GANJIL ");
        }
        else
            System.out.printf(bilangan+" Adalah bilangan GENAP ");
    }
        
}          
    
  
