/* PROGRAM HelloWorld */
/* Penjelasan untuk mencetak "Hello world". Masukan Program ini tidak 
ada. Keluarannya adalah tulisan 'Hello, world' tercetak dilayar */

public class HelloWorld {
	/* DEKLARASI */
	/* tidak ada */
	
	/* ALGORITMA */
	public static void main (String[] args) {
		System.out.print ("Hello, world");
	}
}
