package id.zein_lab.pertemuan.ketujuh;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class BilanganGenap {

    private int bilangan;

    public boolean getHasil(int bilangan) {
        return bilangan % 2 == 0;
    }

}
