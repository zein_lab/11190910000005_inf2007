package id.zein_lab.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class AplikasiPersegiPanjang {

    public static void main(String[] args) {
        //Mencoba konstruktor default
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();

        Scanner in = new Scanner(System.in);
        double panjang, lebar;

        System.out.print("Masukkan Panjang: ");
        panjang = in.nextDouble();

        System.out.print("Masukkan Lebar: ");
        lebar = in.nextDouble();

        PersegiPanjang persegiPanjang = new PersegiPanjang(panjang, lebar);
        persegiPanjang.getInfo();
        System.out.println("Luas : " + persegiPanjang.getLuas());
        System.out.println("Keliling : " + persegiPanjang.getKeliling());
    }

}
