package id.zein_lab.pertemuan.ketujuh;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class ParameterSegitiga {

    private double alas;
    private double tinggi;

    public ParameterSegitiga() {
        System.out.println("Konstruktor Segitiga");

    }

    public ParameterSegitiga(double alas, double tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;

    }

    public double getLuas() {
        return (alas * tinggi) / 2;

    }

    public void getInfo() {
        System.out.println("Kelas Segitiga");
    }

}
