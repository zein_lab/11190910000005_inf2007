package id.zein_lab.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class SequentialSearch {

    public boolean getSeqSearchBoolean(int L[], int n, int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            i = i + 1;
        }
        return L[i] == x;
    }

    public int getSeqSearchIndeks(int L[], int n, int x) {
        int i = 1;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke = " + "" + i + "adalah" + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi ke = " + "" + i + "adalah" + L[i]);
            System.out.println(getSeqSearchIndeks(L, n, x));
        }

        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x, n;
        int L[] = {13, 16, 14, 21, 76, 15};
        n = L.length;
        System.out.println("Masukkan Nilai Yang Dicari =");
        x = in.nextInt();
        SequentialSearch y = new SequentialSearch();
        System.out.println(y.getSeqSearchBoolean(L, n, x));

    }
}
