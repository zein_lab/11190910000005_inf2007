package id.zein_lab.pertemuan.keenam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class InputStreamReaderEx {

    public static void main(String[] args) {
        int bilangan;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Masukkan bilangan: ");
        try {
            bilangan = Integer.parseInt(in.readLine());
            System.out.printf("Bilangan: " + bilangan);
        } catch (IOException ex) {
            System.out.print("error: " + ex.toString());
        }
    }

}
