package id.zein_lab.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class InputScannerEx {

    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan bilangan: ");
        bilangan = in.nextInt();
        System.out.print("Bilangan: " + bilangan);

    }

}
