package id.zein_lab.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class InputJOptionPaneEx {

    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan Bilangan: ");
        bilangan = Integer.parseInt(box);
        System.out.print("Bilangan: " + bilangan);

    }

}
