package id.zein_lab.pertemuan.keduabelas;

/**
 *
 * @author AWIEZ FATHWA ZEIN
 */
public class Tabungan {

    protected int saldo;

    public Tabungan(int saldo) {
        this.saldo = saldo;
    }

    public int ambilUang(int jumlah) {
        saldo = saldo - jumlah;
        return this.saldo;
    }

}
